/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alanmrace.jimzmlvalidator.gui;

import com.alanmrace.jimzmlparser.exceptions.NonFatalParseException;
import com.alanmrace.jimzmlparser.mzml.CVParam;
import com.alanmrace.jimzmlparser.obo.OBOTerm;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 *
 * @author alan.race
 */
public class CVParamModel {

    private ObjectProperty<CVParam> cvParamProperty;

    ObjectProperty<OBOTerm> oboTerm;

    BooleanProperty hasValue;
    StringProperty value;

    BooleanProperty hasUnits;
    ObjectProperty<OBOTerm> units;

    StringProperty cvParamDescription;
    
    public CVParamModel(CVParam cvParam) {
        this.cvParamProperty = new SimpleObjectProperty(cvParam);

        if(cvParam == null) {
            oboTerm = new SimpleObjectProperty(null);
            value = new SimpleStringProperty(null);
            units = new SimpleObjectProperty(null);
            hasValue = new SimpleBooleanProperty();
            hasUnits = new SimpleBooleanProperty();
            
            cvParamDescription = new SimpleStringProperty("");
        } else {
            oboTerm = new SimpleObjectProperty(cvParam.getTerm());
            value = new SimpleStringProperty(cvParam.getValueAsString());
            units = new SimpleObjectProperty(cvParam.getUnits());
            
            hasValue = new SimpleBooleanProperty(cvParam.getTerm().getValueType() != null);
            hasUnits = new SimpleBooleanProperty((cvParam.getTerm().getUnits() != null && !cvParam.getTerm().getUnits().isEmpty()));
            
            cvParamDescription = new SimpleStringProperty(cvParam.toString());
        }

        cvParamProperty.addListener((property, oldValue, newValue) -> {
            if(newValue == null) {
                oboTerm.set(null);
                value.set(null);
                units.set(null);
                hasValue.set(false);
                hasUnits.set(false);
                
                cvParamDescription.setValue("");
            } else {
                oboTerm.set(newValue.getTerm());
                value.set(newValue.getValueAsString());
                units.set(newValue.getUnits());

                hasValue.set(newValue.getTerm().getValueType() != null);
                hasUnits.set((newValue.getTerm().getUnits() != null && !newValue.getTerm().getUnits().isEmpty()));
                
                cvParamDescription.setValue(this.cvParamProperty.get().toString());
            }
        });
        
        oboTerm.addListener((property, oldValue, newValue) -> {
            if (newValue instanceof OBOTerm) {
                try {
                    CVParam newCVParam = CVParam.createCVParam(newValue, null);

                    if (cvParamProperty.get() != null && newCVParam.getClass().equals(cvParamProperty.get().getClass())) {
                        cvParamProperty.get().setTerm(newValue);
                    } else {
                        cvParamProperty.setValue(newCVParam);
                    }
                } catch (NonFatalParseException ex) {
                    // TODO: More than log?
                    Logger.getLogger(CVParamModel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
        value.addListener((property, oldValue, newValue) -> {
            // TODO: Report errors occuring with this - maybe make a function or error listener
            // that MzMLContentEditor can listen to to display error messages
            if((oldValue == null || !oldValue.equals(newValue)) && 
                    cvParamProperty.get() != null)
                try {
                    cvParamProperty.get().setValueAsString(newValue);
                } catch(NumberFormatException ex) {
                    notify(ex);
                }
        });
        
        units.addListener((property, oldValue, newValue) -> {
            if((oldValue == null || !oldValue.equals(newValue)) &&
                    cvParamProperty.get() != null)
                cvParamProperty.get().setUnits(newValue);
        });
    }

    private List<CVParamModelExceptionListner> listeners = new ArrayList<>();
    
    public interface CVParamModelExceptionListner {
        void handleException(Exception ex);
    }
    
    public void addListener(CVParamModelExceptionListner listener) {
        this.listeners.add(listener);
    }
    
    private void notify(Exception ex) {
        listeners.forEach((listener) -> {
            listener.handleException(ex);
        });
    }
    
    public ObjectProperty<CVParam> getCVParamProperty() {
        return cvParamProperty;
    }
    
    public OBOTerm getOBOTerm() {
        return oboTerm.get();
    }

    public ObjectProperty getOBOTermProperty() {
        return oboTerm;
    }

    public String getValue() {
        return value.get();
    }

    public StringProperty getValueProperty() {
        return value;
    }

    public BooleanProperty getHasValueProperty() {
        return hasValue;
    }
    
    public OBOTerm getUnits() {
        return units.get();
    }

    public ObjectProperty getUnitsProperty() {
        return units;
    }
    
    public BooleanProperty getHasUnitsProperty() {
        return hasUnits;
    }
    
    public StringProperty getCVParamDescriptionProperty() {
        return cvParamDescription;
    }
}

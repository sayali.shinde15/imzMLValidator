/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alanmrace.jimzmlvalidator.gui;

import com.alanmrace.jimzmlparser.mzml.MzMLContentWithParams;
import com.alanmrace.jimzmlvalidator.CVMappingRule;
import com.alanmrace.jimzmlvalidator.CVTerm;
import java.io.IOException;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.VBox;

/**
 *
 * @author alan.race
 */
public class CVMappingRuleEditor extends VBox {
    
    @FXML
    VBox cvMappingRuleBox;        
    
    CVMappingRule cvMappingRule;
    MzMLContentWithParams mzMLContent;
    
    public CVMappingRuleEditor() {
        super();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CVMappingRuleEditor.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }
    
    public void initialise(CVMappingRule cvMappingRule, MzMLContentWithParams mzMLContent) {
        this.cvMappingRule = cvMappingRule;
        this.mzMLContent = mzMLContent;
        
        cvMappingRuleBox.getChildren().clear();
        
        for(CVTerm term : cvMappingRule.getCVTerms()) {
            CVTermEditor termEditor = new CVTermEditor();
            termEditor.initialise(term, mzMLContent, cvMappingRule);
            
            cvMappingRuleBox.getChildren().add(termEditor);
        }
    }
    
    protected void addCVParamEditorListChangeListener(ListChangeListener<CVParamEditor> changeListener) {
        for(Node node : cvMappingRuleBox.getChildren()) {
            if(node instanceof CVTermEditor) {
                ((CVTermEditor) node).getCVParamEditorList().addListener(changeListener);
            }
        }
    }
}

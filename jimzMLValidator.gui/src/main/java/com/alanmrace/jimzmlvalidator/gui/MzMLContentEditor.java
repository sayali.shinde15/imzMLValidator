/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alanmrace.jimzmlvalidator.gui;

import com.alanmrace.jimzmlparser.event.CVParamAddRemoveEvent;
import com.alanmrace.jimzmlparser.event.OBOTermCVParamChangeEvent;
import com.alanmrace.jimzmlparser.mzml.CVParam;
import com.alanmrace.jimzmlparser.mzml.HasChildren;
import com.alanmrace.jimzmlparser.mzml.MzML;
import com.alanmrace.jimzmlparser.mzml.MzMLContent;
import com.alanmrace.jimzmlparser.mzml.MzMLContentWithParams;
import com.alanmrace.jimzmlparser.mzml.MzMLReference;
import com.alanmrace.jimzmlparser.mzml.MzMLTag;
import com.alanmrace.jimzmlparser.obo.OBOTerm;
import com.alanmrace.jimzmlvalidator.CVMappingRule;
import com.alanmrace.jimzmlvalidator.DependentCVMappingRule;
import com.alanmrace.jimzmlvalidator.MzMLValidator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.control.Accordion;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author alan.race
 */
public class MzMLContentEditor extends VBox {
  
    @FXML Hyperlink parentHyperlink;
    
    @FXML
    VBox childrenBox;
    
    @FXML
    ScrollPane scrollPane;
    
    @FXML
    VBox scrollPaneBox;
    
    @FXML
    Accordion paramsAccordion;
    
    @FXML
    TitledPane mustPane;
    
    @FXML
    VBox mustCVBox;
    
    @FXML
    TitledPane shouldPane;
    
    @FXML
    VBox shouldCVBox;
    
    @FXML
    TitledPane mayPane;
    
    @FXML
    VBox mayCVBox;
    
    @FXML VBox extraCVBox;
    @FXML TitledPane extraTitledPane;
    
    MzMLContent mzMLContent;
    MzMLValidator validator;
    
    public MzMLContentEditor() {
        super();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/MzMLContentEditor.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        
        mustPane.managedProperty().bind(mustPane.visibleProperty());
        shouldPane.managedProperty().bind(shouldPane.visibleProperty());
        mayPane.managedProperty().bind(mayPane.visibleProperty());
        
        paramsAccordion.expandedPaneProperty().addListener((event) -> {
            // Reinitialise to ensure that values are correct after adding a param in 
            // a MAY section and then trying to edit it in the MUST section
            
            if(paramsAccordion.expandedPaneProperty().getValue() != null)
                if(mzMLContent != null)
                    initialise(validator, mzMLContent);
        });
    }
    
    private List<MzMLTag> getSortedChildren() {
        List<MzMLTag> children = new LinkedList<>();
        ((HasChildren) mzMLContent).addChildrenToCollection(children);

        // Sort the children such that the CVParams are after all other child tags 
        // (opposite order to how it is in the XML, but easier for navagating around
        children.sort((x, y) -> {
            if (x.getClass().equals(y.getClass()) && !(x instanceof CVParam)) {
                return 0;
            }

            if (x instanceof CVParam && !(y instanceof CVParam)) {
                return 1;
            } else if (y instanceof CVParam && !(x instanceof CVParam)) {
                return -1;
            }

            return (children.indexOf(x) > children.indexOf(y)) ? 1 : -1;
        });

        return children;
    }
    
    protected void highlightChild(MzMLTag child) {
        if (child instanceof HasChildren) {
            initialise(validator, (MzMLContent) child);
        }

        if (child instanceof MzMLReference
                && ((MzMLReference) child).getReference() instanceof HasChildren) {
            initialise(validator, (MzMLContent) ((MzMLReference) child).getReference());
        }

        if (child instanceof CVParam) {
            List<CVParamEditor> paramEditorList = findCVParamEditors((CVParam) child);

            // TODO: Make this much more elegant
            if (paramEditorList == null || paramEditorList.isEmpty()) {
                Logger.getLogger(MzMLContentEditor.class.getName()).log(Level.SEVERE, "No CVParamEditor for {0}", child);
            } else {
                CVParamEditor paramEditor = paramEditorList.get(0);

                // TODO: Fix this.
                Node accordion = paramEditor.getParent().getParent().getParent().getParent().getParent().getParent().getParent();

                if (accordion instanceof TitledPane) {
                    final boolean pauseRequired = paramsAccordion.getExpandedPane() == null || !(paramsAccordion.getExpandedPane().equals((TitledPane) accordion));
                    paramsAccordion.setExpandedPane((TitledPane) accordion);

                    Runnable gainFocus = () -> {
                        if (pauseRequired) {
                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException ex) {
                                Logger.getLogger(MzMLContentEditor.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }

                        Platform.runLater(() -> {
                            ensureVisible(scrollPane, accordion);
                            paramEditor.getOBOComboBox().requestFocus();
                        });
                    };

                    new Thread(gainFocus).start();
                }
            }
        }
    }
    
    private int maxChildren = 100;
    private int displayedTo = 0;
    
    private void setUpChildrenList(MzMLContent content) {
        childrenBox.getChildren().clear();
        
        if(mzMLContent instanceof HasChildren) {
            List<MzMLTag> children = getSortedChildren();
            boolean extraButton = false;
            
            if(children.size() > maxChildren) {
                children = children.subList(displayedTo, displayedTo+maxChildren);
                extraButton = true;
            }
            
            displayedTo = children.size();
            
            for(MzMLTag child : children) {
                Hyperlink link = new Hyperlink(child.toString());

                link.setOnAction((event) -> {
                    highlightChild(child);
                });

                childrenBox.getChildren().add(link);
            }
            
            // TODO: Clean up code duplication from below and above
            if(extraButton) {
                Hyperlink extraLink = new Hyperlink("More...");

                extraLink.setOnAction((event) -> {
                    List<MzMLTag> fullChildList = getSortedChildren();
                    boolean needExtraButton = false;
            
                    if(fullChildList.size() > displayedTo+maxChildren) {
                        fullChildList = fullChildList.subList(displayedTo, displayedTo+maxChildren);
                        needExtraButton = true;
                    }
                    
                    for(MzMLTag child : fullChildList) {
                        Hyperlink link = new Hyperlink(child.toString());

                        link.setOnAction((addChildEvent) -> {
                            highlightChild(child);
                        });

                        childrenBox.getChildren().add(link);
                    }
                    
                    displayedTo += maxChildren;
                    
                    childrenBox.getChildren().remove(extraLink);
                    
                    if(needExtraButton) {
                        childrenBox.getChildren().add(extraLink);
                    
                        Runnable gainFocus = () -> {
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException ex) {
                                Logger.getLogger(MzMLContentEditor.class.getName()).log(Level.SEVERE, null, ex);
                            }

                            Platform.runLater(() -> {
                                ensureVisible(scrollPane, extraLink);
                            });
                        };

                        new Thread(gainFocus).start();
                    }
                });

                childrenBox.getChildren().add(extraLink);
            }
        }
    }
    
    public void initialise(MzMLValidator validator, MzMLContent mzMLContent) {
        // Clean up previous listeners so multiple calls are not made
        if(this.mzMLContent != null)
            this.mzMLContent.removeAllListeners();
        System.out.println("Validator: " + validator);
        
        this.validator = validator;
        this.mzMLContent = mzMLContent;
        
        // Set the title of the editor to the new mzML content
        Stage stage = (Stage) getScene().getWindow();
        stage.setTitle(mzMLContent.toString());
        
        // Don't remove or add in listeners for the base tag
        if(!(mzMLContent instanceof MzML) && mzMLContent != null) {        
            mzMLContent.removeAllListeners();

            mzMLContent.addListener((event) -> {
                setUpChildrenList(mzMLContent);

                if(event instanceof OBOTermCVParamChangeEvent || 
                        event instanceof CVParamAddRemoveEvent) {
                    Platform.runLater(() -> {
                        initialise(validator, mzMLContent);
                    });
                }
            });
        }
        
        MzMLTag parent = ((MzMLTag)mzMLContent).getParent();
        
        if(parent != null) {
            parentHyperlink.setText(parent.toString());
            
            parentHyperlink.setOnAction((event) -> {
                initialise(this.validator, (MzMLContent) parent);
            });
        } else {
            parentHyperlink.setText("None");
            parentHyperlink.setOnAction((event) -> {});
        }
        
        setUpChildrenList(mzMLContent);
        
        // Deal with 
        
        if(mzMLContent instanceof MzMLContentWithParams) {
            if(!scrollPaneBox.getChildren().contains(paramsAccordion))
                scrollPaneBox.getChildren().add(paramsAccordion);
            
            mustCVBox.getChildren().clear();
            shouldCVBox.getChildren().clear();
            mayCVBox.getChildren().clear();
            
            // Only show the extra box if it has been opened, otherwise it's a 
            // performance hit
            extraCVBox.getChildren().clear();
            
            if(paramsAccordion.getExpandedPane() != null && 
                    paramsAccordion.getExpandedPane().equals(extraTitledPane)) {    
                CVTermEditor allParams = new CVTermEditor();
                allParams.initialise(null, (MzMLContentWithParams) mzMLContent);
                extraCVBox.getChildren().add(allParams);
            }

//            System.out.println("MzMLContentEditor#initialise(): XPath: " + ((MzMLContentWithParams)mzMLContent).getXPath());

            // Get the rules
            List<CVMappingRule> rules = this.validator.getRulesFromXPath(((MzMLContentWithParams)mzMLContent).getXPath());

            //List<CVMappingRule> ruleList = new ArrayList<>(rules);
            //ruleList.sort(c);

            for(CVMappingRule rule : rules) {
                VBox vBox;

                switch(rule.getRequirementLevel()) {
                    case MUST:
                        vBox = mustCVBox;
                        break;
                    case SHOULD:
                        vBox = shouldCVBox;
                        break;
                    case MAY:
                    default:
                        vBox = mayCVBox;
                        break;
                }

                CVMappingRuleEditor ruleEditor = new CVMappingRuleEditor();
                ruleEditor.initialise(rule, (MzMLContentWithParams)mzMLContent);

//                ruleEditor.addCVParamEditorListChangeListener((ListChangeListener.Change<? extends CVParamEditor> event) -> {
//                    while (event.next()) {
//                        if (event.wasAdded()) {
//                            System.out.println("RuleEditorEvent: " + event);
//                            
//                            for(CVParamEditor editor : event.getAddedSubList()) {
//                                System.out.println("Processing editor " + editor + " (" + editor.getCVParam() + ")");
//                                
//                                List<CVParamEditor> editors = findCVParamEditors(editor.getCVParam());
//
//                                if(editors.size() > 0) {
//                                    System.out.println("Disable all but first, make first binding for " + editor);
//                                }
//                            }
//                        }
//                    }
//                });
                
                boolean added = false;

                if(rule instanceof DependentCVMappingRule) {
//                    System.out.println(rule + " is dependent on " + ((DependentCVMappingRule) rule).getDependentTerm());

                    CVMappingRuleEditor insertionLocation = null;
                    int indexLocation = -1;
                    
                    for(Node child : vBox.getChildren()) {
                        if(child instanceof CVMappingRuleEditor) {                            
                            for(Node grandchild : ((CVMappingRuleEditor) child).cvMappingRuleBox.getChildren()) {                                
                                if(grandchild instanceof CVTermEditor) {    
                                    for(Node greatGrandChild : ((CVTermEditor) grandchild).cvParamBox.getChildren()) {
                                        if(greatGrandChild instanceof CVParamEditor) {
                                            OBOTerm term = ((CVParamEditor) greatGrandChild).paramModel.oboTerm.get();
                                            
                                            if(term != null && ((DependentCVMappingRule) rule).getDependentTerm().getID().equals(term.getID())) {
                                                insertionLocation = ((CVMappingRuleEditor) child);
                                                indexLocation = insertionLocation.cvMappingRuleBox.getChildren().indexOf(grandchild) + 1;
                                                
                                                break;
                                            }
                                        }
                                    }
                                }
                                
                                if(insertionLocation != null)
                                    break;
                            }
                        }
                        
                        if(insertionLocation != null)
                            break;
                    }
                    
                    if(insertionLocation != null && indexLocation >= 0) {
//                        System.out.println("[Try] Inserting " + insertionLocation + " at location " + indexLocation);
                        
                        insertionLocation.cvMappingRuleBox.getChildren().addAll(indexLocation, ruleEditor.cvMappingRuleBox.getChildren());
                        
//                        for (Node termEditor : ruleEditor.cvMappingRuleBox.getChildren()) {
//                            if (termEditor instanceof CVTermEditor) {
//                                System.out.println("Inserting " + termEditor + " at location " + indexLocation);
//                                insertionLocation.cvMappingRuleBox.getChildren().add(indexLocation++, termEditor);
//                            }
//                        }
                        
                        added = true;
                    }
                }

                if(!added)
                    vBox.getChildren().add(ruleEditor);
            }
        } else {
            scrollPaneBox.getChildren().remove(paramsAccordion);
        }
    }
    
    private static void ensureVisible(ScrollPane pane, Node node) {
        Bounds viewport = pane.getViewportBounds();
        double contentHeight = pane.getContent().getBoundsInLocal().getHeight();
        // Must include the parent in the calculation as this is a grandchild of the 
        // scroll pane
        // TODO: Traverse up the tree until the scrollpane is reached calculating the exact min / max
        double nodeMinY = node.getBoundsInParent().getMinY() + node.getParent().getBoundsInParent().getMinY();
        double nodeMaxY = node.getBoundsInParent().getMaxY() + node.getParent().getBoundsInParent().getMaxY();
        double viewportMinY = (contentHeight - viewport.getHeight()) * pane.getVvalue();
        double viewportMaxY = viewportMinY + viewport.getHeight();
        
        if (nodeMinY < viewportMinY) {
            pane.setVvalue(nodeMinY / (contentHeight - viewport.getHeight()));
        } else if (nodeMaxY > viewportMaxY) {
            pane.setVvalue((nodeMaxY - viewport.getHeight()) / (contentHeight - viewport.getHeight()));
        }
        
        node.requestFocus();
    }

    private List<CVParamEditor> findCVParamEditors(CVParam param) {
        List<CVMappingRuleEditor> ruleEditors = new LinkedList<>();
        List<CVParamEditor> paramEditorList = new ArrayList<>();
        
        mustCVBox.getChildren().stream().filter((node) -> (node instanceof CVMappingRuleEditor)).forEachOrdered((node) -> {
            ruleEditors.add((CVMappingRuleEditor)node);
        });
        
        shouldCVBox.getChildren().stream().filter((node) -> (node instanceof CVMappingRuleEditor)).forEachOrdered((node) -> {
            ruleEditors.add((CVMappingRuleEditor)node);
        });
        
        mayCVBox.getChildren().stream().filter((node) -> (node instanceof CVMappingRuleEditor)).forEachOrdered((node) -> {
            ruleEditors.add((CVMappingRuleEditor)node);
        });
        
        for(CVMappingRuleEditor ruleEditor : ruleEditors) {
            VBox vBox = (VBox) ruleEditor.getChildren().get(0);
            
            for(Node termEditor : vBox.getChildren()) {
                if(termEditor instanceof CVTermEditor) {
                    VBox cvParamBox = (VBox) termEditor.lookup("#cvParamBox");

                    for(Node node : cvParamBox.getChildren()) {
                        if(node instanceof CVParamEditor) {
                            if(((CVParamEditor) node).getCVParam() != null &&
                                    ((CVParamEditor) node).getCVParam() == param)
                                paramEditorList.add((CVParamEditor) node);
                        }
                    }
                }
            }
        }        
        
        return paramEditorList;
    }
}

package com.alanmrace.jimzmlvalidator.gui;

import com.alanmrace.jimzmlparser.imzml.ImzML;
import com.alanmrace.jimzmlparser.mzml.MzMLContentWithParams;
import com.alanmrace.jimzmlparser.parser.ImzMLHandler;
import com.alanmrace.jimzmlvalidator.CVMapping;
import com.alanmrace.jimzmlvalidator.ImzMLValidator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.alanmrace.jimzmlvalidator.JimzMLValidator;
import com.alanmrace.jimzmlvalidator.options.ValidatorOptions;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainApp extends Application {

    ImzMLValidatorController controller;
    ValidatorOptions options;

    @Override
    public void start(Stage stage) throws Exception {
        //Parent root = FXMLLoader.load(getClass().getResource("/fxml/Scene.fxml"));
        //loadTagEdit(stage);
        //loadCVTermEditor(stage);
        //System.out.println("Working Directory = " +
        //      System.getProperty("user.dir"));
        
        
        loadMain(stage);
    }

    private void loadCVTermEditor(Stage stage) throws Exception {
        MzMLContentEditor vBox = new MzMLContentEditor();
        
        Scene scene = new Scene(vBox);
        scene.getStylesheets().add("/styles/Styles.css");

        stage.setTitle("imzMLValidator");
        stage.setScene(scene);
        stage.show();
        
        stage.minHeightProperty().set(400);
        stage.minWidthProperty().set(600);

        String filename = "D:\\Alan\\Julia\\2017-02-22_mouse_brain_DHB_10fliter_30att__LM716_mz600-900_240000_120000.imzML";
        ImzML imzML = ImzMLHandler.parseimzML(filename);
        MzMLContentWithParams mzMLContent = imzML.getSpectrumList().getSpectrum(0);
        mzMLContent = imzML.getScanSettingsList().getScanSettings(0);
        String xPath = "/mzML/scanSettingsList/scanSettings";

        List<CVMapping> cvMappingList = ImzMLValidator.getDefaultMappingList();
        ImzMLValidator validator = new ImzMLValidator();
        validator.setFile(filename);
        //vBox.initialise(cvMappingList.get(1).getCVMappingRuleList().getCVMappingRule("imzml_scansettings_must"), mzMLContent);
        vBox.initialise(validator, mzMLContent);
        
        /*boolean initialised = false;
        
        for (CVMapping cvMap : cvMappingList) {
            for (CVMappingRule rule : cvMap.getCVMappingRuleList()) {
                
                System.out.println(rule.getScopePath());
                if (rule.getScopePath().equals(xPath)) {
                    if(initialised)
                        vBox.initialise(rule.getCVTerms().get(rule.getCVTerms().size()-1), mzMLContent);
                    initialised = true;
                }
            }
        }*/

    }

    private void loadTagEdit(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/IssueEditor.fxml"));
        Parent root = loader.load();

        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");

        stage.setTitle("imzMLValidator");
        stage.setScene(scene);
        stage.show();

        IssueEditorController issueController = loader.<IssueEditorController>getController();

        //issueController.
    }
    
    
    
    private void loadMain(Stage stage) throws Exception {
        //options = ValidatorOptions.loadFromFile("jimzMLValidator.json");
        ValidatorOptions options = JimzMLValidator.getOptions(JimzMLValidator.DEFAULT_OPTIONS_FILE_NAME);
        JimzMLValidator.downloadAllFiles(options);


        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Scene.fxml"));
        Parent root = loader.load();

        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");

        stage.setTitle("imzMLValidator");
        stage.setScene(scene);
        stage.show();
        
        controller = loader.<ImzMLValidatorController>getController();
        controller.setOptions(options);
        
        stage.setOnCloseRequest((event) -> {
            controller.stop();

            try {
                stop();
            } catch (Exception ex) {
                Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    //@Override
    //public void stop(){
    //    controller.stop();
    //}
    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Working Directory = " + System.getProperty("user.dir"));

        launch(args);
    }

}

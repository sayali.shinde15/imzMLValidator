/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alanmrace.jimzmlvalidator.gui;

import com.alanmrace.jimzmlparser.event.CVParamAddedEvent;
import com.alanmrace.jimzmlparser.event.CVParamRemovedEvent;
import com.alanmrace.jimzmlparser.event.MzMLEvent;
import com.alanmrace.jimzmlparser.mzml.CVParam;
import com.alanmrace.jimzmlparser.mzml.MzMLContentWithParams;
import com.alanmrace.jimzmlvalidator.CVMappingRule;
import com.alanmrace.jimzmlvalidator.CVTerm;
import com.alanmrace.jimzmlvalidator.exceptions.CVMappingRuleException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author alan.race
 */
public class CVTermEditor extends VBox {

    @FXML
    Label termSuccess;
    
    @FXML
    Label termDescription;
    
    @FXML
    VBox cvParamBox;
    
    protected MzMLContentWithParams mzMLContent;
    protected CVTerm cvTerm;
    
    protected CVMappingRule cvMappingRule;
    
    protected ObservableList<CVParamEditor> cvParamEditors = FXCollections.observableArrayList();
    
    public CVTermEditor() {
        super();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CVTermEditor.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }   
    
    public void initialise(CVTerm cvTerm, MzMLContentWithParams mzMLContent) {      
        initialise(cvTerm, mzMLContent, null);
    }
    
    public void initialise(CVTerm cvTerm, MzMLContentWithParams mzMLContent, CVMappingRule cvMappingRule) {
        this.mzMLContent = mzMLContent;
        this.cvTerm = cvTerm;
        
        this.cvMappingRule = cvMappingRule;
        
        if(cvMappingRule != null) {
            termSuccess.setOnMouseClicked((event) -> {
                    if(cvMappingRule.isIgnoring(cvTerm))
                        cvMappingRule.ignoreCVTerm(cvTerm, false);
                    else
                        cvMappingRule.ignoreCVTerm(cvTerm);
                    
                    updateTermSuccess();
                });
        }
        
        mzMLContent.addListener((MzMLEvent event) -> {
            if(event instanceof CVParamAddedEvent) {
                CVParam cvParam = ((CVParamAddedEvent) event).getCVParam();
                
                if(cvTerm == null || cvTerm.isOBOTermCovered(cvParam.getTerm())) {
                    boolean foundnull = false;
                    boolean foundCVParam = false;
                    
                    CVParamEditor nullEditor = null;
                    
                    for (Iterator<CVParamEditor> iterator = cvParamEditors.iterator(); iterator.hasNext();) {
                        CVParamEditor cvParamEditor = iterator.next();
                        
                        if(((CVParamAddedEvent) event).getCVParam().equals(cvParamEditor.getCVParam()))
                            foundCVParam = true;
                        
                        if(cvParamEditor.getCVParam() == null) {
                            nullEditor = cvParamEditor;
                            foundnull = true;
                        }
                    }
                    
                    if(!foundCVParam && !foundnull && (cvTerm == null || cvTerm.isRepeatable())) {
                        addCVParamEditor(cvParam, false);
                    } else if(!foundCVParam && nullEditor != null) {
                        // Update the null editor rather than adding a new one to the end
                        nullEditor.setCVParam(cvParam);
                        foundnull = false;
                    }
                    
                    if(!foundnull && (cvTerm == null || cvTerm.isRepeatable())) {
                        addCVParamEditor(null, false);
                    }
                }
            } else if(event instanceof CVParamRemovedEvent) {
                // Check to see if the CVParam
                
                for (Iterator<CVParamEditor> iterator = cvParamEditors.iterator(); iterator.hasNext();) {
                    CVParamEditor cvParamEditor = iterator.next();
                    
                    // If the CVParam already exists in the list, then remove it
                    // Have to use equality comparison because of overriden equals();
                    if(((CVParamRemovedEvent) event).getCVParam() == cvParamEditor.getCVParam()) {
                        if(cvTerm != null && cvTerm.isRepeatable()) {
                            iterator.remove();
                            //cvParamEditors.remove(cvParamEditor);
                            cvParamBox.getChildren().remove(cvParamEditor);
                        } else {
                            cvParamEditor.setCVParam(null);
                        }
                    }
                }
            }
            
            updateTermSuccess();
        });
        
        update();
    }
    
    public ObservableList<CVParamEditor> getCVParamEditorList() {
        return cvParamEditors;
    }
    
    protected void addCVParamEditor(CVParam cvParam, boolean disable) {
//        System.out.println("addCVParamEditor: Adding CVParamEditor");
        CVParamEditor cvParamEditor = new CVParamEditor();
        cvParamEditor.initialise(cvTerm, mzMLContent, cvParam);
        
        cvParamEditor.setDisable(disable);
        
        if(cvTerm == null || cvTerm.isRepeatable()) {
            // Add option to delete the CVParam
            Button deleteButton = new Button();
            deleteButton.setText("X");

            deleteButton.setOnAction((event) -> {
                // TODO: Delete
                CVParam cvParamToRemove = cvParamEditor.getCVParam();

                if(cvParamToRemove != null) {
                    mzMLContent.removeCVParam(cvParamToRemove);
                } else {
                    System.out.println("--- content --- ");
                    for(CVParam cvParam2 : mzMLContent.getCVParamList())
                        System.out.println(cvParam2 + " (" + System.identityHashCode(cvParam2) + ")");
                }
            });

            cvParamEditor.getChildren().add(deleteButton);
        }
        
        cvParamEditors.add(cvParamEditor);
        cvParamBox.getChildren().add(cvParamEditor);
    }
    
    protected void updateTermSuccess() {
        if(cvTerm == null) {
            termSuccess.setText("");
            return;
        }
        
        try {
            if(cvMappingRule != null && cvMappingRule.isIgnoring(cvTerm)) {
                termSuccess.setText("[Ignoring] ✓");
                System.out.println("Check: " + cvTerm.check(mzMLContent));
            } else {
                if(cvTerm.check(mzMLContent))
                    termSuccess.setText("✓");
                else
                    termSuccess.setText("✖");
            }
        } catch (CVMappingRuleException ex) {
            termSuccess.setText("✖");
        }
    }
    
    protected void update() {
        updateTermSuccess();
        
        if(cvTerm != null) {
            termDescription.setText(cvTerm.getDescription());
            
            if(cvTerm.getOBOTerm() != null)
                termDescription.setTooltip(new Tooltip(cvTerm.getOBOTerm().getDescription()));
        }
        
        cvParamBox.getChildren().clear();
        cvParamEditors.clear();
                
        if(mzMLContent != null) {
            boolean atLeastOneAdded = false;
            
            List<CVParam> cvParamList = mzMLContent.getCVParamList();
            
            if(cvParamList != null) {
                for(CVParam cvParam : cvParamList) {
                    if(cvTerm == null || cvTerm.isOBOTermCovered(cvParam.getTerm())) {
                        addCVParamEditor(cvParam, false);

                        atLeastOneAdded = true;
                    }
                }
            }
            
            if(cvTerm == null || cvTerm.isRepeatable()) {
                addCVParamEditor(null, false);
                
                atLeastOneAdded = true;
            }
            
            if(!atLeastOneAdded) {
                try {
                    // TODO: Check if the CVParam is part of a ReferenceableParamGroup
                    if(cvTerm.check(mzMLContent)) {
                        CVParam cvParam = mzMLContent.getCVParamOrChild(cvTerm.getOBOTerm().getID());
                        
                        addCVParamEditor(cvParam, true);
                        atLeastOneAdded = true;
                    }
                } catch (CVMappingRuleException ex) {
                    // Do nothing
                }
                
                if(!atLeastOneAdded)
                    addCVParamEditor(null, false);
            }
        }
    }
}

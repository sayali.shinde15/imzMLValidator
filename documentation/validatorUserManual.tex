%\documentclass[10pt,a4paper]{article}
\documentclass{achemso}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{pifont}

\usepackage{etoolbox}

\usepackage[dvipsnames]{xcolor}

\usepackage[ampersand]{easylist}

%\usepackage{fullpage}

\newcommand{\includeImage}[2]{\includegraphics[width=#1\textwidth]{#2}} 
%\newcommand{\includeImage}[2]{\includegraphics[width=#1\textwidth,natwidth=610,natheight=642,keepaspectratio]{#2}}

\usepackage{soul, color}
\newcommand{\todo}[1]{\hl{TODO: #1}}
\newcommand{\etal}{{\it et al.}}

\title{imzML validator user manual}
\author{Alan M. Race}
\email{alan.race@uni-bayreuth.de}
\affiliation[Bayreuth]
{Universit\"{a}t Bayreuth, Bayreuth, Germany}

\author{Andreas R\"{o}mpp} 
\email{andreas.roempp@uni-bayreuth.de}
\affiliation[Bayreuth]
{Universit\"{a}t Bayreuth, Bayreuth, Germany}

\newcommand\tab[1][1cm]{\hspace*{#1}}
\newcommand{\oboterm}[2]{(#1) #2} 

\newcommand{\newoboterm}[2]{{\oboterm{#1}{#2}}}
\newcommand{\changedoboterm}[2]{{\oboterm{#1}{#2}}}
%\newcommand{\newoboterm}[2]{{\bf \oboterm{#1}{#2}}}
%\newcommand{\changedoboterm}[2]{{\em \oboterm{#1}{#2}}}

\newcommand{\included}{\textcolor{ForestGreen}{[Included]}}
\newcommand{\partincluded}{\textcolor{Dandelion}{[Partially included]}}
\newcommand{\notincluded}{\textcolor{red}{[Not included]}}

\begin{document}
\maketitle

\pagebreak

\section{Validation}

Upon starting the imzML validator tool, the interface shown in Figure \ref{fig:loadInterface} will be presented. To select an imzML file to validate, either drag and drop the imzML file or click on the blue box.

\begin{figure}
\centering
	\includeImage{0.6}{Figures/imzMLValidator.png}
	\caption{Loading interface.}
	\label{fig:loadInterface}
\end{figure}

Any issues that are encountered while the file is loading into the validator, will automatically be added to the `Issue List' (number 4 in Figure \ref{fig:mainInterface}). To perform a full validation, the `Validate' button must be clicked. Full validation of an imzML file consists of 5 steps shown in Figure \ref{fig:validationProcess}. Validation of an mzML file consists of the first three steps shown in Figure \ref{fig:validationProcess}.

\begin{figure}
\centering
	\includeImage{1.0}{Figures/validationProcess.png}
	\caption{Validation process.}
	\label{fig:validationProcess}
\end{figure}

If the `Skip data integrity check' checkbox (visible in Figure \ref{fig:additionalValidation} and accessible by clicking on the `Options' dropdown menu) is ticked prior to clicking on the `Validate' button, the hash of the ibd file will not be performed. This can save significant time in the validation process (depending on the size of the data) if it is already known that the binary data has not been altered.

The interface after validating an invalid imzML file is shown in Figure \ref{fig:mainInterface}. The colour of the issue indicates the severity (red being the most severe and yellow the least), and also indicates where in the validation process this may have occurred (Figure \ref{fig:validationProcess}). Common issues and their resolutions are shown in Figure \ref{fig:resolvingIssues}.

\begin{figure}
\centering
	\includeImage{0.8}{Figures/imzMLValidator_interface.png}
	\caption{Main interface shown after an invalid imzML file has been validated. 1) TIC image. 2) Report showing key metadata. 3) Full imzML metadata tree. 4) Issue list. 5) Red issue. 6) Orange issue. 7) Yellow issue. 8) Save button.}
	\label{fig:mainInterface}
\end{figure}



\begin{figure}
\centering
	\includeImage{1.0}{Figures/resolvingIssues.png}
	\caption{Errors that can occur as part of the validation process and the means of resolving them using the validator.}
	\label{fig:resolvingIssues}
\end{figure}

\section{Editing}

When double clicking any issue in the `Issue list' (shown in Figure \ref{fig:mainInterface}), or selecting any part of the imzML tree, the editor window will be shown for the selected imzML tag, shown in Figure \ref{fig:editorInterface}. In this window, CV parameters can be added, modified and removed. CV parameters are organised into the categories `Must', `Should' and `May' depending on the requirement level as specified in the mapping file(s) validated against. Any additional parameters that are not covered within the mapping file(s) are included within the `Extra' category.

Rules which have not been satisfied will be displayed with a `\ding{54}' whereas satisfied rules are denoted by a `\checkmark', providing the user with continuous feedback as to the validity of any given (i)mzML element.

\begin{figure}
\centering
	\includeImage{0.8}{Figures/editor_interface.png}
	\caption{Editor interface. 1) List of metadata associated with the imzML metadata tag being edited. 2) Editable list of metadata associated with imzML metadata tag being edited organised into Must, Should and May categories based on mapping files. Rules which have not been satisfied will be displayed with a `\ding{54}' whereas satisfied rules are denoted by a `\checkmark'. 3) Drop-down selection box for modifying CV parameters. 4) Edit box for altering the value associated with a given CV parameter.}
	\label{fig:editorInterface}
\end{figure}


\section{Conditional Validation}

It is possible to validate against additional sets of rules that are not part of the imzML specification. Such sets of rules could cover, for example, institutional metadata requirements or project metadata requirements. To include a new mapping file, or conditional mapping file (discussed below), these must first be added into the validator, as shown in Figure \ref{fig:additionalValidation}, by selecting the `Options' drop-down menu. Once added into the validator, the mapping files can be turned on (`\ding{54}' in `Include' column) or off (empty `Include' column) by selecting or deselecting them from the list.

\begin{figure}
\centering
	\includeImage{0.8}{Figures/additionalValidation.png}
	\caption{Interface to add in additional mapping files or conditional mapping files.}
	\label{fig:additionalValidation}
\end{figure}

Conditional mapping files provide rules to validate against which are dependent on the present metadata. This enables MALDI specific rules to be validated, only when MALDI data is being validated (by the presence of the MALDI CV parameter in the metadata). 

\subsection{Conditional mapping XML}

The simplest way to add in custom conditional mapping rules is to create conditional mapping XML, based on the mapping files used in mzML and imzML. An example rule is shown in Figure \ref{fig:conditionalMapping}. 

\begin{figure}
\centering
	\includeImage{1.0}{Figures/conditionalXML.png}
	\caption{Conditional mapping file XML example.}
	\label{fig:conditionalMapping}
\end{figure}

Choices for combination logic are:

\begin{itemize}
	\item OR - Validate that at least one of the terms are present within the imzML header
	\item AND - Validate that all of the terms are present within the imzML header
	\item XOR - Validate that only one of the terms are present within the imzML header
\end{itemize}

Choices for exists are:

\begin{itemize}
	\item none - Validate that the selected term does not exist within the imzML header
	\item term - Validate that the exact term exists within the imzML header
	\item child - Validate that the term or a child term exists within the imzML header
\end{itemize}


\subsection{Conditional mapping Java}

More complex mapping rules, that cannot be implemented within the conditional mapping XML file, can be developed in Java and included within the validator. To do this, the `Condition' interface should be extended, and a JAR file created. The JAR file can then be loaded (and selected/deselected) in the same way as the conditional mapping XML shown in Figure \ref{fig:additionalValidation}.

Extending the `Condition' interface only requires implementing one method, {\em public boolean check(MzML mzML)}, which is given the entire (i)mzML object containing all metadata and should return true if the rule is valid, and false if the rule is not met.

\end{document}
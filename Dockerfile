FROM ubuntu

MAINTAINER Alan Race

ENV DEBIAN_FRONTEND noninteractive

ENV TZ Europe/Berlin

# Update repository sources list
RUN apt-get update

# Install software necessary for Git
RUN apt-get install -y apt-utils wget git-all

# Install software necessary for Java
RUN apt-get install -y openjdk-8-jdk openjfx

# Install maven 3.5.4
RUN wget http://ftp.halifax.rwth-aachen.de/apache/maven/maven-3/3.5.4/binaries/apache-maven-3.5.4-bin.tar.gz
RUN tar xzvf apache-maven-3.5.4-bin.tar.gz

ENV PATH="/apache-maven-3.5.4/bin:${PATH}"

# Install software necessary for latex
RUN apt-get install -y texlive-latex-base texlive-latex-extra

# Set up SSH
RUN which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )
RUN eval $(ssh-agent -s)
RUN mkdir -p ~/.ssh
RUN chmod 700 ~/.ssh
RUN ssh-keyscan gitlab.com >> ~/.ssh/known_hosts

# Install pandoc version 2.2.3.2
RUN wget https://github.com/jgm/pandoc/releases/download/2.2.3.2/pandoc-2.2.3.2-1-amd64.deb
RUN dpkg -i pandoc-2.2.3.2-1-amd64.deb

# Install python3
RUN apt-get install -y python3 python3-pip
RUN pip3 install --no-cache-dir requests

# Get gitlab-release
RUN git clone https://github.com/inetprocess/gitlab-release.git
RUN cp /gitlab-release/gitlab-release /usr/bin/


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alanmrace.jimzmlvalidator;

import com.alanmrace.jimzmlparser.exceptions.InvalidFormatIssue;
import com.alanmrace.jimzmlvalidator.exceptions.UnexpectedCVParamMappingRuleException;

import java.util.List;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import com.alanmrace.jimzmlparser.exceptions.Issue;
import com.alanmrace.jimzmlparser.exceptions.MissingReferenceIssue;
import com.alanmrace.jimzmlvalidator.CVMappingRule.RequirementLevel;
import com.alanmrace.jimzmlvalidator.exceptions.CombinationMappingRuleException;
import com.alanmrace.jimzmlvalidator.exceptions.InvalidXMLIssue;
import java.util.logging.Level;

/**
 *
 * @author Alan
 */
public class ImzMLValidatorTest {

    private static final Logger logger = Logger.getLogger(ImzMLValidatorTest.class.getName());

    public final static String SIMPLEST_IMZML = "/simplest.imzML";
    public final static String SIMPLEST_MAPPING_IMZML = "/simplest_mapping.imzML";
    public final static String SIMPLEST_MAPPING_MAYFAIL_IMZML = "/simplest_mapping_MAYFail.imzML";
    public final static String SIMPLEST_MAPPING_INCORRECTREF_IMZML = "/simplest_mapping_IncorrectReference.imzML";
    public final static String SIMPLEST_MAPPING_DOUBLEID_IMZML = "/simplest_mapping_DuplicateID.imzML";
    public final static String SIMPLEST_MAPPING_DUPLICATEUNREPEATABLE_IMZML = "/simplest_mapping_DuplicateUnrepeatable.imzML";
    public final static String SIMPLEST_MAPPING_VALUECHECK_IMZML = "/simplest_mapping_ValueCheck.imzML";

    public ImzMLValidatorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    
    protected boolean performValidation(String filename) {
        ImzMLValidator instance = new ImzMLValidator();
        instance.setFile(filename);

        instance.registerImzMLValidatorListener(new ImzMLValidatorListener() {
            @Override
            public void startingStep(ImzMLValidator.ValidatorStep step) {

            }

            @Override
            public void finishingStep(ImzMLValidator.ValidatorStep step) {

            }

            @Override
            public void issueFound(Issue issue) {
                //if(issue instanceof UnexpectedCVParamMappingRuleException)
                logger.info(issue.toString());
                logger.log(Level.INFO, "{0}: {1}", new Object[]{issue.getIssueTitle(), issue.getIssueMessage()});
            }
        });

        return instance.validate();
    }

    /**
     * Test of validate method, of class ImzMLValidator.
     */
    @Test
    public void testValidateDuplicateUnrepeatableImzML() {
        System.out.println();
        System.out.println("------------");
        System.out.println("Validating dupliacte unrepeatable imzML");
        System.out.println("------------");
        System.out.println();
        //ImzMLValidator instance = new ImzMLValidator(ImzMLValidatorTest.class.getResource(IMZML_RESOURCE).getPath());

        boolean result = performValidation(ImzMLValidatorTest.class.getResource(SIMPLEST_MAPPING_DUPLICATEUNREPEATABLE_IMZML).getPath());

        assertEquals(false, result);
    }
    
    /**
     * Test of validate method, of class ImzMLValidator.
     */
    @Test
    public void testValidateSimplestImzML() {
        System.out.println();
        System.out.println("------------");
        System.out.println("Validating simplest imzML");
        System.out.println("------------");
        System.out.println();
        //ImzMLValidator instance = new ImzMLValidator(ImzMLValidatorTest.class.getResource(IMZML_RESOURCE).getPath());

        boolean result = performValidation(ImzMLValidatorTest.class.getResource(SIMPLEST_MAPPING_IMZML).getPath());

        assertEquals(false, result);
    }
    
    @Test
    public void testValidateSimplestMAYFailImzML() {
        System.out.println();
        System.out.println("------------");
        System.out.println("Validating simplest MAY Fail imzML");
        System.out.println("------------");
        System.out.println();
        
        ImzMLValidator instance = new ImzMLValidator();
        instance.setFile(ImzMLValidatorTest.class.getResource(SIMPLEST_MAPPING_MAYFAIL_IMZML).getPath());
        
        instance.registerImzMLValidatorListener(new ImzMLValidatorListener() {
            @Override
            public void startingStep(ImzMLValidator.ValidatorStep step) {

            }

            @Override
            public void finishingStep(ImzMLValidator.ValidatorStep step) {

            }

            @Override
            public void issueFound(Issue issue) {
                if(issue instanceof UnexpectedCVParamMappingRuleException)
                    logger.info(issue.getIssueMessage());
                else if(issue instanceof CombinationMappingRuleException && 
                        !((CombinationMappingRuleException)issue).getCVMappingRule().getRequirementLevel().equals(RequirementLevel.MUST)) {
                    logger.info(issue.getIssueMessage());
                } else
                    fail("Unexpected issue found: " + issue);
            }
        });

        boolean result = instance.validate();
        logger.log(Level.INFO, "CVParam 1: {0}", instance.mzML.getInstrumentConfigurationList().getInstrumentConfiguration(0).getCVParam(0));
        logger.log(Level.INFO, "CVParam 2: {0}", instance.mzML.getInstrumentConfigurationList().getInstrumentConfiguration(0).getCVParam(1));
        assertEquals(false, result);
    }
    
    @Test
    public void testValidateSimplestIncorrectReferenceImzML() {
        System.out.println();
        System.out.println("------------");
        System.out.println("Validating simplest incorrect reference imzML");
        System.out.println("------------");
        System.out.println();
        
        ImzMLValidator instance = new ImzMLValidator();
        instance.setFile(ImzMLValidatorTest.class.getResource(SIMPLEST_MAPPING_INCORRECTREF_IMZML).getPath());
        
        instance.registerImzMLValidatorListener(new ImzMLValidatorListener() {
            @Override
            public void startingStep(ImzMLValidator.ValidatorStep step) {

            }

            @Override
            public void finishingStep(ImzMLValidator.ValidatorStep step) {

            }

            @Override
            public void issueFound(Issue issue) {
                if(issue instanceof InvalidXMLIssue || 
                        issue instanceof MissingReferenceIssue)
                    logger.info(issue.getIssueMessage());
                else if(issue instanceof CombinationMappingRuleException && 
                        !((CombinationMappingRuleException)issue).getCVMappingRule().getRequirementLevel().equals(RequirementLevel.MUST)) {
                    logger.info(issue.getIssueMessage());
                }
                else {
                    System.out.println(" ---- " + issue.getIssueTitle() + " ---- ");
                    System.out.println(issue.getIssueMessage());
                    fail("Unexpected issue found: " + issue);
                }
            }
        });

        boolean result = instance.validate();
        
        assertEquals(false, result);
    }
    
    @Test
    public void testValidateSimplestDoubleIDImzML() {
        System.out.println();
        System.out.println("------------");
        System.out.println("Validating simplest double ID imzML");
        System.out.println("------------");
        System.out.println();
        
        ImzMLValidator instance = new ImzMLValidator();
        instance.setFile(ImzMLValidatorTest.class.getResource(SIMPLEST_MAPPING_DOUBLEID_IMZML).getPath());
        
        instance.registerImzMLValidatorListener(new ImzMLValidatorListener() {
            @Override
            public void startingStep(ImzMLValidator.ValidatorStep step) {

            }

            @Override
            public void finishingStep(ImzMLValidator.ValidatorStep step) {

            }

            @Override
            public void issueFound(Issue issue) {
                if(issue instanceof InvalidXMLIssue || 
                        issue instanceof MissingReferenceIssue)
                    logger.info(issue.getIssueMessage());
                else if(issue instanceof CombinationMappingRuleException && 
                        !((CombinationMappingRuleException)issue).getCVMappingRule().getRequirementLevel().equals(RequirementLevel.MUST)) {
                    logger.info(issue.getIssueMessage());
                } else {
                    
                    System.out.println(" ---- " + issue.getIssueTitle() + " ---- ");
                    System.out.println(issue.getIssueMessage());
                    fail("Unexpected issue found: " + issue);
                }
            }
        });

        boolean result = instance.validate();
        
        assertEquals(false, result);
    }
    
    @Test
    public void testValidateSimplestValueCheckImzML() {
        System.out.println();
        System.out.println("------------");
        System.out.println("Validating simplest value check imzML");
        System.out.println("------------");
        System.out.println();
        
        ImzMLValidator instance = new ImzMLValidator();
        instance.setFile(ImzMLValidatorTest.class.getResource(SIMPLEST_MAPPING_VALUECHECK_IMZML).getPath());
        
        instance.registerImzMLValidatorListener(new ImzMLValidatorListener() {
            @Override
            public void startingStep(ImzMLValidator.ValidatorStep step) {

            }

            @Override
            public void finishingStep(ImzMLValidator.ValidatorStep step) {

            }

            @Override
            public void issueFound(Issue issue) {
                if(issue instanceof InvalidFormatIssue)
                    logger.info(issue.getIssueMessage());
                else {
                    if(issue instanceof UnexpectedCVParamMappingRuleException) {
                        System.out.println(((UnexpectedCVParamMappingRuleException) issue).getCVMappingRule().getDescription());
                    }
                    
                    System.out.println(" ---- " + issue.getIssueTitle() + " ---- ");
                    System.out.println(issue.getIssueMessage());
                    fail("Unexpected issue found: " + issue);
                }
            }
        });

        boolean result = instance.validate();
        
        assertEquals(false, result);
    }

}

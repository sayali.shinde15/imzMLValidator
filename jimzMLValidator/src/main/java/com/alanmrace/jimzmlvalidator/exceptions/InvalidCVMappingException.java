/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alanmrace.jimzmlvalidator.exceptions;

/**
 *
 * @author Alan
 */
public class InvalidCVMappingException extends Exception {
    
    public InvalidCVMappingException(String message) {
        super(message);
    }
}

package com.alanmrace.jimzmlvalidator;

import com.alanmrace.jimzmlparser.exceptions.Issue;
import com.alanmrace.jimzmlparser.obo.HTTPOBOLoader;
import com.alanmrace.jimzmlparser.obo.OBO;
import com.alanmrace.jimzmlvalidator.JimzMLValidatorCommand.OutputType;
import com.alanmrace.jimzmlvalidator.options.ValidatorOptions;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Alan Race
 */
public class JimzMLValidator {
	private static final Logger LOGGER = Logger.getLogger(JimzMLValidator.class.getName());

	private static String DEFAULT_OPTIONS_FILE_RESOURCE = "/options/defaultOptions.json";
	public static String DEFAULT_OPTIONS_FILE_NAME = "jimzMLValidator.json";

	public static void downloadAllFiles(ValidatorOptions options) throws IOException {
        if(options.downloadLatestSchemasOnStart()) {
            downloadFile(options.getMzMLSchemaURI(), "Schemas");
            downloadFile(options.getMzMLIDXSchemaURI(), "Schemas");
            downloadFile(options.getImzMLSchemaURI(), "Schemas");
            downloadFile(options.getImzMLAllSchemaURI(), "Schemas");
        }

        if(options.downloadLatestMappingFilesOnStart()) {
            List<String> mappingURIs = options.getMappingURIs();
            List<String> conditionaMappingURIs = options.getConditionalMappingURIs();

            for(String mappingURI : mappingURIs) {
                downloadFile(mappingURI, "MappingFiles");
            }

            for(String mappingURI : conditionaMappingURIs) {
                downloadFile(mappingURI, "ConditionalMappingFiles");
            }
        }

        if(options.downloadLatestOBOsOnStart()) {
            OBO.loadOntologyFromURL(options.getIMSOBOURI());
            OBO.loadOntologyFromURL(options.getMSOBOURI());
        }
    }

    private static void downloadFile(String location, String outputPath) throws IOException {
        URL website = new URL(location);
        String fileName = location.substring(location.lastIndexOf('/')+1);

        ReadableByteChannel rbc = Channels.newChannel(website.openStream());

        // Make sure that the folder to save the data to exists
        File outputFolder = Paths.get(outputPath).toFile();
        outputFolder.mkdirs();

        FileOutputStream fos = new FileOutputStream(Paths.get(outputPath, fileName).toFile());
        fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
    }

    private static boolean isFirstRun() {
        return !(new File(DEFAULT_OPTIONS_FILE_NAME).isFile());
    }

	public static ValidatorOptions getOptions(String optionsFile) throws IOException {
        boolean isFirstRun = false;

        // Check if the options file exists already, if not then copy over the default file and load it
        if(optionsFile.equals(DEFAULT_OPTIONS_FILE_NAME) && isFirstRun()) {
            InputStream in = JimzMLValidator.class.getResourceAsStream(DEFAULT_OPTIONS_FILE_RESOURCE);

            OutputStream out = new FileOutputStream(new File(DEFAULT_OPTIONS_FILE_NAME));

            byte[] buffer = new byte[1024];
            int len;

            while ((len = in.read(buffer)) != -1) {
                out.write(buffer, 0, len);
            }

            in.close();
            out.close();

            isFirstRun = true;
        }

        ValidatorOptions options = ValidatorOptions.loadFromFile(optionsFile);
        options.isFirstRun(isFirstRun);

        return options;
    }

    public static void main(String[] args) {
        final List<Issue> issueList = new ArrayList<>();
        JimzMLValidatorCommand arguments = new JimzMLValidatorCommand();
        
        JCommander jc = new JCommander(arguments);
        jc.setProgramName("jimzMLValidator");
        jc.setCaseSensitiveOptions(false);
        
        try {
            jc.parse(args);
        } catch (ParameterException pex) {
            arguments.help = true;

            LOGGER.log(Level.SEVERE, pex.getLocalizedMessage(), pex);
        }

        if (arguments.help) {
            jc.usage();
            System.exit(0);
        }
        
        String fileToValidate = arguments.fileToValidate;
        
        String extension = "";

        int i = fileToValidate.lastIndexOf('.');
        if (i > 0) {
            extension = fileToValidate.substring(i+1);
        }

        try {
            ValidatorOptions options = getOptions(arguments.optionsFile);

            downloadAllFiles(options);
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Cannot open options file " + arguments.optionsFile + " so reverting to default settings");
        }

        MzMLValidator validator = null;
        
        if(extension.equalsIgnoreCase("imzML")) {
            validator = new ImzMLValidator();
        } else if (extension.equalsIgnoreCase("mzML")) {
            validator = new MzMLValidator();
        } else {
            LOGGER.log(Level.SEVERE, "File must be either imzML or an mzML.");
            
            System.exit(0);
        }
        
        validator.setFile(fileToValidate);

        validator.registerImzMLValidatorListener(new ImzMLValidatorListener() {
            @Override
            public void startingStep(ImzMLValidator.ValidatorStep step) {
                if(arguments.verbose)
                    LOGGER.log(Level.INFO, "Starting step {0}.", step);
            }

            @Override
            public void finishingStep(ImzMLValidator.ValidatorStep step) {
                if(arguments.verbose)
                    LOGGER.log(Level.INFO, "Finishing step {0}.", step);
            }

            @Override
            public void issueFound(Issue issue) {
                issueList.add(issue);
            }
        });
        
        validator.validate();
        
        StringBuilder outputBuilder = new StringBuilder();
        
        if(arguments.outputType.equals(OutputType.TEXT)) {
            for(Issue issue : issueList) { 
                outputBuilder.append(issue.getIssueTitle()).append("\n");
                outputBuilder.append(issue.getIssueMessage()).append("\n").append("\n");
            }
        } else if(arguments.outputType.equals(OutputType.JSON)) {
            outputBuilder.append("{\"issues\": [");
            
            boolean first = true;
            
            for(Issue issue : issueList) {
                if(first) {
                    first = false;
                } else {
                    outputBuilder.append(",");
                }
                
                String escapedString = issue.getIssueMessage().replace("\"", "\\\"");
                escapedString = escapedString.replace("\n", "\\n");
                escapedString = escapedString.replace("\t", "\\t");
                
                outputBuilder.append("\n").append("\t{");
                outputBuilder.append("\"level\": \"").append(issue.getIssueLevel()).append("\", ");
                outputBuilder.append("\"title\": \"").append(issue.getIssueTitle()).append("\", ");
                outputBuilder.append("\"message\": \"").append(escapedString).append("\"}");
            }
            
            outputBuilder.append("\n").append("]}");
        }
        
        String output = outputBuilder.toString();
        
        try {
	        if(arguments.output != null && !arguments.output.isEmpty()) {
	           outputToFile(arguments.output, output);
	        } else {
	            System.out.write(output.getBytes());
	        }
        } catch (IOException ex) {
            Logger.getLogger(JimzMLValidator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static void outputToFile(String filename, String output) throws IOException {
    	 try (OutputStream outputStream = new FileOutputStream(new File(filename))) {
         	outputStream.write(output.getBytes());
         } 
    }
}

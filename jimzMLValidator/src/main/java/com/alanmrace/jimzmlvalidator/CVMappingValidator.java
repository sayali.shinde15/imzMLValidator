package com.alanmrace.jimzmlvalidator;

import com.alanmrace.jimzmlparser.exceptions.InvalidXPathException;
import com.alanmrace.jimzmlparser.obo.OBO;
import com.alanmrace.jimzmlvalidator.exceptions.CVMappingRuleException;
import com.alanmrace.jimzmlvalidator.exceptions.UncheckableMappingRuleException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.alanmrace.jimzmlparser.exceptions.Issue;
import com.alanmrace.jimzmlparser.mzml.MzML;
import com.alanmrace.jimzmlparser.mzml.MzMLContentWithParams;
import com.alanmrace.jimzmlparser.mzml.MzMLTag;

/**
 *
 * @author Alan Race
 */
public class CVMappingValidator {
    private static final Logger logger = Logger.getLogger(CVMappingValidator.class.getName());
    
    protected CVMapping cvMapping;
    protected OBO obo;
    
    private List<CVMappingValidatorListener> listeners = new LinkedList<>();
    
    public CVMappingValidator(CVMapping cvMapping) {
        this.cvMapping = cvMapping;
    }
    
    public void setOBO(OBO obo) {
        this.obo = obo;
    }
    
    public void registerCVMappingValidatorListener(CVMappingValidatorListener listener) {
        listeners.add(listener);
    }
    
    protected void notifyCVMappingValidatorListeners(Issue issue) {
        listeners.forEach(listener -> listener.issueFound(issue));
    }
    
    public void validate(MzML mzML) {
        for(CVMappingRule rule : cvMapping.cvMappingRuleList) {
            logger.log(Level.FINE, "Checking {0}", rule);
            
            rule.setOBO(obo);
            
            try {
                String scopePath = rule.getScopePath();
                
                // Fix the odd cases where /cvParam was included as part of the scope
                // Was this intentional?
                if(scopePath.endsWith("/cvParam"))
                    scopePath = scopePath.replace("/cvParam", "");
                    
                if(rule.getCVElementPath().replaceFirst(scopePath, "").equals("/cvParam/@accession")) {
                    Collection<MzMLTag> elementsToCheck = new LinkedList<>();
                    
                    mzML.addElementsAtXPathToCollection(elementsToCheck, rule.getScopePath());

                    for(MzMLTag content : elementsToCheck) {
                        try {
                            if(content instanceof MzMLContentWithParams)
                                rule.check((MzMLContentWithParams) content);
                            else {
                                // TODO: Add in an error to say that we are trying to check something that 
                                // cannot be checked
                                logger.log(Level.SEVERE, "Not checking the content {0} because does not contain CVParams", content);
                            }
                        } catch (CVMappingRuleException ex) {
                            notifyCVMappingValidatorListeners(ex);
                        }
                    }
                } else {
                    throw new InvalidXPathException("Unimplemented XPath check: " + rule.getCVElementPath(), rule.getCVElementPath());
                }
            } catch (InvalidXPathException ex) {                
                Issue issue = new UncheckableMappingRuleException(rule, ex);

                notifyCVMappingValidatorListeners(issue);
            }
        }
    }
}

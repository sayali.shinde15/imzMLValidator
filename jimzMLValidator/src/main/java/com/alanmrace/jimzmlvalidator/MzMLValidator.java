package com.alanmrace.jimzmlvalidator;

import com.alanmrace.jimzmlparser.exceptions.FatalParseException;
import com.alanmrace.jimzmlparser.exceptions.Issue;
import com.alanmrace.jimzmlparser.mzml.MzML;
import com.alanmrace.jimzmlparser.obo.OBO;
import com.alanmrace.jimzmlparser.parser.MzMLHeaderHandler;
import com.alanmrace.jimzmlparser.parser.ParserListener;
import com.alanmrace.jimzmlvalidator.exceptions.InvalidXMLIssue;
import com.alanmrace.jimzmlvalidator.exceptions.UncheckableMappingRuleException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import com.alanmrace.jimzmlvalidator.options.ValidatorOptions;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Alan Race
 */
public class MzMLValidator implements CVMappingValidatorListener, ParserListener {
    private static final Logger logger = Logger.getLogger(MzMLValidator.class.getName());
    
    protected static String[] xPathList = {
        "/mzML/fileDescription/fileContent",
        "/mzML/fileDescription/sourceFileList/sourceFile",
        "/mzML/fileDescription/contact",
//        "/mzML/referenceableParamGroupList/referenceableParamGroup",
        "/mzML/sampleList/sample",
        "/mzML/softwareList/software",
        "/mzML/scanSettingsList/scanSettings",
        "/mzML/scanSettingsList/scanSettings/targetList/target",
        "/mzML/instrumentConfigurationList/instrumentConfiguration",
        "/mzML/instrumentConfigurationList/instrumentConfiguration/componentList/source",
        "/mzML/instrumentConfigurationList/instrumentConfiguration/componentList/analyzer",
        "/mzML/instrumentConfigurationList/instrumentConfiguration/componentList/detector",
        "/mzML/dataProcessingList/dataProcessing/processingMethod",
        "/mzML/run",
        "/mzML/run/spectrumList/spectrum",
        "/mzML/run/spectrumList/spectrum/scanList",
        "/mzML/run/spectrumList/spectrum/scanList/scan",
        "/mzML/run/spectrumList/spectrum/scanList/scan/scanWindowList/scanWindow",
        "/mzML/run/spectrumList/spectrum/precursorList/precursor/isolationWindow",
        "/mzML/run/spectrumList/spectrum/precursorList/precursor/selectedIonList/selectedIon",
        "/mzML/run/spectrumList/spectrum/precursorList/precursor/activation",
        "/mzML/run/spectrumList/spectrum/productList/product/isolationWindow",
        "/mzML/run/spectrumList/spectrum/binaryDataArrayList/binaryDataArray",
        "/mzML/run/chromatogramList/chromatogram",
        "/mzML/run/chromatogramList/chromatogram/precursor/isolationWindow",
        "/mzML/run/chromatogramList/chromatogram/precursor/selectedIonList/selectedIon",
        "/mzML/run/chromatogramList/chromatogram/precursor/activation",
        "/mzML/run/chromatogramList/chromatogram/product/isolationWindow",
        "/mzML/run/chromatogramList/chromatogram/binaryDataArrayList/binaryDataArray",
    };
    
    
    protected List<CVMapping> cvMappingList = new LinkedList<>();
    protected List<ConditionalMapping> conditionalMappingList = new LinkedList<>();
    
    protected String filename;

    protected boolean foundIssue;
    protected List<ImzMLValidatorListener> listeners;
    private boolean okToContinue = true;
    
    protected MzML mzML = null;
    protected OBO obo;
    
    /**
     * Default constructor. Sets up the default CVMappingList
     */
    public MzMLValidator() {
        this.listeners = new LinkedList<>();

        cvMappingList = MzMLValidator.getDefaultMappingList();
    }

    public MzMLValidator(ValidatorOptions options) {
        // TODO: Create from options
    }
    
    public static List<CVMapping> getDefaultMappingList() {
        List<CVMapping> cvMappingList = new LinkedList<>();

        cvMappingList.add(CVMappingHandler.parseCVMapping(ImzMLValidator.class.getResourceAsStream("/mapping/ms-mapping.xml"), OBO.getOBO()));

        // Additional mapping file to remove errors when sourceFile parameters are used. The mzML validator moved this to an object rule
        cvMappingList.add(CVMappingHandler.parseCVMapping(ImzMLValidator.class.getResourceAsStream("/mapping/sourceFile-may-mapping.xml"), OBO.getOBO()));

        return cvMappingList;
    }
    
    public void setFile(String filename) {
        this.filename = filename;
    }
    
     /**
     * When supplying an in memory ImzML instance, the mapping will be tested
     * against the in memory version rather than what is stored on disk. XML
     * validity against the schema will still be performed against the on disk
     * imzML.
     *
     * @param filename
     * @param mzML
     */
    public void setFile(String filename, MzML mzML) {
        setFile(filename);
        
        this.mzML = mzML;
    }
    
    @Override
    public void issueFound(Issue issue) {
        // Don't bother with the uncheckable rules 
        if (issue instanceof UncheckableMappingRuleException) {
            return;
        }

        addIssue(issue);
    }
    
    protected void addIssue(Issue issue) {
        foundIssue = true;

        if (issue.getIssueMessage().contains("cvc-complex-type.2.4.b")) {
            okToContinue = false;
        }

        notifyImzMLValidatorListeners(issue);
    }
    
    public void registerImzMLValidatorListener(ImzMLValidatorListener listener) {
        this.listeners.add(listener);
    }

    protected void notifyImzMLValidatorListeners(ImzMLValidator.ValidatorStep step, boolean start) {
        logger.log(Level.FINE, "{0} {1}", new Object[]{((start)? "Started" : "Finished"),  step});
        
        listeners.forEach(listener -> {
            if (start) {
                listener.startingStep(step);
            } else {
                listener.finishingStep(step);
            }
        });
    }

    protected void notifyImzMLValidatorListeners(Issue issue) {
        listeners.forEach(listener -> listener.issueFound(issue));
    }
    
    protected InputStream getXSDStream() {
        return MzMLValidator.class.getResourceAsStream("/xsd/mzML1.1.1_idx.xsd");
    }
    
    protected boolean validateXMLSchema() {
        logger.log(Level.FINE,"Evaluating XML Schema");

        SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
        factory.setResourceResolver(new ResourceResolver());

        Source xsdFile = new StreamSource(getXSDStream());
        File xmlFile = new File(filename);

        okToContinue = true;

        try {
            Schema schema = factory.newSchema(xsdFile);
            Validator validator = schema.newValidator();

            validator.setErrorHandler(new DefaultHandler() {
                @Override
                public void warning(SAXParseException exception) throws SAXException {
                    logger.log(Level.INFO, "WARNING: {0}", exception.getLocalizedMessage());
                    addIssue(new InvalidXMLIssue(exception.getLocalizedMessage(), exception));
                }

                @Override
                public void fatalError(SAXParseException exception) throws SAXException {
                    logger.log(Level.INFO, "FATAL: {0}", exception.getLocalizedMessage());
                    addIssue(new InvalidXMLIssue(exception.getLocalizedMessage(), exception));
                }

                @Override
                public void error(SAXParseException exception) throws SAXException {
                    logger.log(Level.INFO, "ERROR: {0}", exception.getLocalizedMessage());
                    addIssue(new InvalidXMLIssue(exception.getLocalizedMessage(), exception));
                }
            });

            try (FileReader fileReader = new FileReader(xmlFile)){
            	Source source = new SAXSource(new InputSource(fileReader));

                validator.validate(source);
            } catch (IOException ex) {
                Logger.getLogger(MzMLValidator.class.getName()).log(Level.SEVERE, null, ex);
                addIssue(new InvalidXMLIssue(ex.getLocalizedMessage(), ex));

                okToContinue = false;
            }

        } catch (org.xml.sax.SAXException ex) {
            Logger.getLogger(MzMLValidator.class.getName()).log(Level.SEVERE, null, ex);
        }

        return okToContinue;
    }
    
    protected boolean validateXML() {
        logger.log(Level.FINE,"Evaluating XML validity");

        try {
            if (mzML == null) {
                mzML = MzMLHeaderHandler.parsemzMLHeader(filename, false, this);
            } else {
                // TODO: Rethink this - currently the mzML variable is not updated as
                // this would remove the ability to select the issue and have this
                // select the correct item in the treeItemMap in ImzMLValidatorController
                MzMLHeaderHandler.parsemzMLHeader(filename, false, this);
            }

            obo = mzML.getOBO();
        } catch (FatalParseException ex) {
            Logger.getLogger(MzMLValidator.class.getName()).log(Level.SEVERE, null, ex);

            // Stop here as there's no point continuing if this exception is thrown,
            // as possible causes are:
            //      - SAXException
            //      - FileNotFoundException
            //      - IOException
            //      - ParserConfigurationException
            addIssue(ex.getIssue());

            return false;
        }

        return true;
    }
    
    public List<CVMappingRule> getRulesFromXPath(String xPath) {
        List<CVMappingRule> ruleList = new ArrayList<>();

        // TODO: This is a repeat of the code in the validate() method. Clean up.
        List<CVMapping> mapList = new ArrayList<>();
        mapList.addAll(cvMappingList);
        
        // Check and add all conditional mapping files
        for(ConditionalMapping conditionalMapping : conditionalMappingList) 
            mapList.addAll(conditionalMapping.getMappingList(mzML));

        //List<CVMapping> consolidatedCVMappingList = consolidateCVMappingList(mapList);
        for (CVMapping mapping : mapList) {
            for (CVMappingRule rule : mapping.getCVMappingRuleList()) {
                if (rule.getScopePath().equals(xPath)) {
                    ruleList.add(rule);
                }
            }
        }

        return ruleList;
    }

    public void addCVMapping(CVMapping mapping) {
        this.cvMappingList.add(mapping);
    }
    
    public void removeCVMapping(CVMapping mapping) {
        this.cvMappingList.remove(mapping);
    }

    public void addCVMapping(List<CVMapping> mappingList) {
        this.cvMappingList.addAll(mappingList);
    }
    
    public void clearAllMapping() {
        this.cvMappingList.clear();
        this.conditionalMappingList.clear();
    }
    
    public void addConditionalMapping(ConditionalMapping conditionalMapping) {
        this.conditionalMappingList.add(conditionalMapping);
    }
    
    public void removeConditionalMapping(ConditionalMapping conditionalMapping) {
        this.conditionalMappingList.remove(conditionalMapping);
    }

    protected static CVMapping consolidateCVMappingList(List<CVMapping> cvMappingList) {
        HashMap<String, CVMappingRule> hashList = new HashMap<>();
        CVMapping consolidatedCVMapping = new CVMapping();

//        CVMapping mayMap = new CVMapping("may.xsd", "may.xsd", "1.0.0");
//        consolidatedCVMapping.add(mayMap);

        for (String xPath : xPathList) {
            CVMappingRule newRule = new CVMappingRule("MAY_" + xPath, xPath + "/cvParam/@accession",
                    CVMappingRule.RequirementLevel.MAY, xPath, CVMappingRule.CombinationLogic.OR);

            hashList.put(xPath, newRule);

            consolidatedCVMapping.addCVMappingRule(newRule);
        }

        for (CVMapping map : cvMappingList) {
            CVMapping mapCopy = new CVMapping(map);

            for (CVMappingRule rule : map.getCVMappingRuleList()) {
                String scopePath = rule.getScopePath();
                
                // Fix for the error in the ms-mapping.xml file:
                //      scopePath="/mzML/fileDescription/contact/cvParam"
                if(scopePath.endsWith("/cvParam"))
                    scopePath = scopePath.replace("/cvParam", "");
                
                CVMappingRule mayRule = hashList.get(scopePath);
                
                if (mayRule == null) {
                    logger.log(Level.SEVERE, "Unexpected scope {0}", rule.getScopePath());
                } else {
                    for (CVTerm term : rule.getCVTerms()) {
                        mayRule.addCVTerm(term);
                    }

                    if (rule.requirementLevel.equals(CVMappingRule.RequirementLevel.MAY)) {
                        //logger.log(Level.WARNING, "Removed {0}: {1}", new Object[] {rule.getDescription(), mapCopy.getCVMappingRuleList().removeCVMappingRule(rule)});
                        mapCopy.getCVMappingRuleList().removeCVMappingRule(rule);
                    }
                }
            }

            for(CVMappingRule rule : mapCopy.getCVMappingRuleList())
                consolidatedCVMapping.addCVMappingRule(rule);
//            consolidatedCVMappingList.add(mapCopy);
        }

        return consolidatedCVMapping;
    }
    
    public boolean validate() {
        foundIssue = false;

        // If we fail at this point then no point continuing
        notifyImzMLValidatorListeners(ImzMLValidator.ValidatorStep.VALIDATE_XML, true);

        if (!validateXMLSchema()) {
//            return false;
        }

        notifyImzMLValidatorListeners(ImzMLValidator.ValidatorStep.VALIDATE_XML, false);
        notifyImzMLValidatorListeners(ImzMLValidator.ValidatorStep.VALIDATE_IMZML, true);

        if (!validateXML()) {
//            return false;
        }

        notifyImzMLValidatorListeners(ImzMLValidator.ValidatorStep.VALIDATE_IMZML, false);

        // Validate OBO mapping 
        notifyImzMLValidatorListeners(ImzMLValidator.ValidatorStep.VALIDATE_MAPPING, true);

        List<CVMapping> mapList = new LinkedList<>();
        mapList.addAll(cvMappingList);
        
        // Check and add all conditional mapping files
        for(ConditionalMapping conditionalMapping : conditionalMappingList) 
            mapList.addAll(conditionalMapping.getMappingList(mzML));
        
        CVMapping consolidatedCVMapping = consolidateCVMappingList(mapList);

        CVMappingValidator validator = new CVMappingValidator(consolidatedCVMapping);
        validator.setOBO(obo);
        validator.registerCVMappingValidatorListener(this);

        validator.validate(mzML);

        notifyImzMLValidatorListeners(ImzMLValidator.ValidatorStep.VALIDATE_MAPPING, false);

        notifyImzMLValidatorListeners(ImzMLValidator.ValidatorStep.COMPLETE, false);
        
        // If there are no issues, then all is okay
        return !foundIssue;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alanmrace.jimzmlvalidator;

import com.alanmrace.jimzmlparser.exceptions.InvalidXPathException;
import com.alanmrace.jimzmlparser.mzml.CVParam;
import com.alanmrace.jimzmlparser.mzml.MzML;
import com.alanmrace.jimzmlparser.mzml.MzMLContentWithParams;
import com.alanmrace.jimzmlparser.mzml.MzMLTag;
import com.alanmrace.jimzmlparser.obo.OBOTerm;
import java.util.Collection;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alan.race
 */
public class DefaultCondition implements Condition {
    public enum ConditionExistence {
        NONE,
        TERM,
        CHILD
    }
    
    ConditionExistence existence;
    String scopePath;
    OBOTerm term;
    
    public DefaultCondition(ConditionExistence existence, String scopePath, OBOTerm term) {
        this.existence = existence;
        this.scopePath = scopePath;
        this.term = term;
    }
    
    public OBOTerm getOBOTerm() {
        return term;
    }
    
    @Override
    public boolean check(MzML mzML) {
        try {
            Collection<MzMLTag> elementsToCheck = new LinkedList<>();
            
            mzML.addElementsAtXPathToCollection(elementsToCheck, scopePath);
            
            if(elementsToCheck.isEmpty())
                return false;
            
            for(MzMLTag mzMLTag : elementsToCheck) {                
                if(mzMLTag instanceof MzMLContentWithParams) {
                    CVParam param;
                    
                    switch(existence) {
                        case NONE:
                            param = ((MzMLContentWithParams) mzMLTag).getCVParam(term.getID());
                            
                            if(param == null)
                                return true;
                            
                            break;
                        case TERM:
                            param = ((MzMLContentWithParams) mzMLTag).getCVParam(term.getID());
                            
                            if(param != null)
                                return true;
                            break;
                        case CHILD:
                            param = ((MzMLContentWithParams) mzMLTag).getCVParamOrChild(term.getID());
                            
                            if(param != null)
                                return true;
                            break;
                    }
                    
                }
            }
        } catch (InvalidXPathException ex) {
            Logger.getLogger(DefaultCondition.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        return false;
    }
}

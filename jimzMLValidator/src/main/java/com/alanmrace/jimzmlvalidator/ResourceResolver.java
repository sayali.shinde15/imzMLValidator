package com.alanmrace.jimzmlvalidator;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;

/**
 *
 * @author Alan Race
 */
public class ResourceResolver implements LSResourceResolver {
	
	private static final Logger logger = Logger.getLogger(ResourceResolver.class.getName());

    public ResourceResolver() {
    }

    @Override
    public LSInput resolveResource(String type, String namespaceURI, String publicId, String systemId, String baseURI) {
        if(systemId.endsWith(".xsd"))
            systemId = "/xsd/" + systemId;
        
        InputStream resourceAsStream = ResourceResolver.class.getResourceAsStream(systemId);
        
        return new Input(publicId, systemId, resourceAsStream);
    }

    public class Input implements LSInput {

        private String publicId;

        private String systemId;

        private BufferedInputStream inputStream;

        public Input(String publicId, String sysId, InputStream input) {
            this.publicId = publicId;
            this.systemId = sysId;
            this.inputStream = new BufferedInputStream(input);
        }
        
        @Override
        public String getPublicId() {
            return publicId;
        }

        @Override
        public void setPublicId(String publicId) {
            this.publicId = publicId;
        }

        @Override
        public String getBaseURI() {
            return null;
        }

        @Override
        public InputStream getByteStream() {
            return null;
        }

        @Override
        public boolean getCertifiedText() {
            return false;
        }

        @Override
        public Reader getCharacterStream() {
            return null;
        }

        @Override
        public String getEncoding() {
            return null;
        }

        @Override
        public String getStringData() {
            synchronized (inputStream) {
                try {
                    byte[] input = new byte[inputStream.available()];
                    int numBytesRead = inputStream.read(input);

                    if(numBytesRead <= 0)
                        return "";
                    
                    // Return contents
                    return new String(input);
                } catch (IOException e) {
                	logger.log(Level.SEVERE, null, e);
                	
                    return null;
                }
            }
        }

        @Override
        public void setBaseURI(String baseURI) {
        	// Not implemented as not required to read XSD.
        }

        @Override
        public void setByteStream(InputStream byteStream) {
        	// Not implemented as not required to read XSD.
        }

        @Override
        public void setCertifiedText(boolean certifiedText) {
        	// Not implemented as not required to read XSD.
        }

        @Override
        public void setCharacterStream(Reader characterStream) {
        	// Not implemented as not required to read XSD.
        }

        @Override
        public void setEncoding(String encoding) {
        	// Not implemented as not required to read XSD.
        }

        @Override
        public void setStringData(String stringData) {
        	// Not implemented as not required to read XSD.
        }

        @Override
        public String getSystemId() {
            return systemId;
        }

        @Override
        public void setSystemId(String systemId) {
            this.systemId = systemId;
        }

        public BufferedInputStream getInputStream() {
            return inputStream;
        }

        public void setInputStream(BufferedInputStream inputStream) {
            this.inputStream = inputStream;
        }
    }
}

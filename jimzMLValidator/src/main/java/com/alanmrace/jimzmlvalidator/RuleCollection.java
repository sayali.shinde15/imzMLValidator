/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alanmrace.jimzmlvalidator;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author alan.race
 */
public class RuleCollection implements HasMappingRule {

    String id;
    List<CVMappingRule> mappingRuleList = new ArrayList<>();
    
    public RuleCollection(String id) {
        this.id = id;
    }
    
    @Override
    public void addCVMappingRule(CVMappingRule mappingRule) {
        mappingRuleList.add(mappingRule);
    }
    
    List<CVMappingRule> getRules() {
        return mappingRuleList;
    }
}
